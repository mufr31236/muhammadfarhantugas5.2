import {View, Text, Image} from 'react-native';
import React from 'react';

const Pemesanan = () => {
  return (
    <View style={{flex: 1}}>
      <Image
        style={{width: 24, height: 24, tintColor: 'black'}}
        source={require('../assets/icon/back_arrow_ic.png')}
      />
    </View>
  );
};

export default Pemesanan;
