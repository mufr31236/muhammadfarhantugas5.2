import {View, Text, Image} from 'react-native';
import React from 'react';

const Profile = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <View
        style={{
          backgroundColor: 'white',
          alignItems: 'center',
          paddingTop: 50,
          paddingBottom: 30,
        }}>
        <Image
          style={{height: 96, width: 96}}
          source={require('../assets/image/profile_img.png')}
        />
        <Text
          style={{
            fontSize: 20,
            fontWeight: '600',
            color: '#050152',
            marginVertical: 5,
          }}>
          Agil Bani
        </Text>
        <Text
          style={{
            fontSize: 10,
            fontWeight: '500',
            color: '#A8A8A8',
            marginVertical: 5,
          }}>
          gilagil@gmail.com
        </Text>
        <View
          style={{
            borderRadius: 20,
            width: 60,
            backgroundColor: '#F6F8FF',
            alignItems: 'center',
            marginTop: 20,
          }}>
          <Text
            onPress={() => {
              navigation.navigate('profilenavigation', {screen: 'edit'});
            }}
            style={{
              color: '#050152',
              paddingHorizontal: 5,
              paddingVertical: 5,
              fontSize: 12,
            }}>
            Edit
          </Text>
        </View>
      </View>
      <View
        style={{
          backgroundColor: 'white',
          marginHorizontal: 20,
          marginVertical: 10,
          paddingVertical: 10,
        }}>
        <Text
          style={{
            color: '#000000',
            marginLeft: '20%',
            fontSize: 16,
            marginVertical: 10,
          }}>
          About
        </Text>
        <Text
          style={{
            color: '#000000',
            marginLeft: '20%',
            fontSize: 16,
            marginVertical: 10,
          }}>
          Terms & Condition
        </Text>
        <Text
          onPress={() => {
            navigation.navigate('faq');
          }}
          style={{
            color: '#000000',
            marginLeft: '20%',
            fontSize: 16,
            marginVertical: 10,
          }}>
          FAQ
        </Text>
        <Text
          style={{
            color: '#000000',
            marginLeft: '20%',
            fontSize: 16,
            marginVertical: 10,
          }}>
          History
        </Text>
        <Text
          style={{
            color: '#000000',
            marginLeft: '20%',
            fontSize: 16,
            marginVertical: 10,
          }}>
          Setting
        </Text>
      </View>
      <View
        style={{
          backgroundColor: 'white',
          marginHorizontal: 20,
          marginVertical: 10,
          paddingVertical: 10,
          alignItems: 'center',
        }}>
        <View style={{flexDirection: 'row'}}>
          <Image
            source={require('../assets/icon/log_out_ic.png')}
            style={{height: 24, width: 24, marginHorizontal: 5}}
          />
          <Text
            onPress={() => {
              navigation.navigate('authnavigation');
            }}
            style={{color: '#EA3D3D', fontWeight: '500', fontSize: 16}}>
            Log out
          </Text>
        </View>
      </View>
    </View>
  );
};

export default Profile;
