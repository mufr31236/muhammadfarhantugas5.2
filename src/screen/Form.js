import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
  FlatList,
} from 'react-native';
import React, {useState} from 'react';
import CheckBox from '@react-native-community/checkbox';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/AntDesign';

const Form = ({navigation, route}) => {
  const [merek, setMerek] = useState('');
  const [warna, setWarna] = useState('');
  const [ukuran, setUkuran] = useState('');
  const [catatan, setCatatan] = useState('');

  const [options, setOptions] = useState([
    {
      name: 'Ganti Sol Sepatu',
      isChecked: false,
    },
    {
      name: 'Jahit Sepatu',
      isChecked: false,
    },
    {
      name: 'Repaint Sepatu',
      isChecked: false,
    },
    {
      name: 'Cuci Sepatu',
      isChecked: false,
    },
  ]);
  const {products} = useSelector(state => state.products);
  const dispatch = useDispatch();

  const handleCheckbox = item => {
    console.log('item  : ', item);
    const updateData = options.map(x =>
      x.name === item.name ? {...x, isChecked: !x.isChecked} : x,
    );
    console.log('data : ', updateData);
    setOptions(updateData);
  };

  const storeData = () => {
    const date = new Date();
    console.log('produk : ', products);
    const dataProduct = [...products];
    const data = {
      id: date.getMilliseconds(),
      merek: merek,
      warna: warna,
      ukuran: ukuran,
      options: options,
      catatan: catatan,
    };
    dataProduct.push(data);
    dispatch({type: 'ADD_PRODUCT', data: dataProduct});
    navigation.navigate('keranjang');
  };

  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 15,
            paddingVertical: 10,
            shadowOpacity: 1,
            shadowRadius: 4,
            shadowColor: 'black',
            elevation: 4,
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <Image
              style={{marginRight: 10}}
              source={require('../assets/icon/black_back_arrow_ic.png')}
            />
          </TouchableOpacity>

          <Text
            style={{
              color: '#201F26',
              fontSize: 18,
              fontWeight: '700',
              tintColor: 'black',
            }}>
            Formulir Pemesanan
          </Text>
        </View>
        <View style={{marginTop: 30, marginHorizontal: 20}}>
          <Text style={{color: '#BB2427', fontSize: 12, fontWeight: '600'}}>
            Merek
          </Text>
          <TextInput
            placeholder="Masukkan Merk barang"
            onChangeText={text => setMerek(text)}
            style={{
              marginTop: 15,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 15,
            }}>
            Warna
          </Text>
          <TextInput
            placeholder="Warna Barang, cth : Merah - Putih "
            onChangeText={text => setWarna(text)}
            style={{
              marginTop: 15,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 15,
            }}>
            Ukuran
          </Text>
          <TextInput
            placeholder="Cth : S, M, L / 39,40"
            onChangeText={text => setUkuran(text)}
            style={{
              marginTop: 15,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 15,
            }}>
            Photo
          </Text>
          <View
            style={{
              borderRadius: 10,
              borderColor: '#BB2427',
              flexDirection: 'column',
              borderWidth: 1,
              paddingVertical: 20,
              alignItems: 'center',
              marginVertical: 20,
              height: 84,
              width: 84,
            }}>
            <Image
              source={require('../assets/icon/camera_ic.png')}
              style={{height: 24, width: 24}}
            />
            <Text style={{color: '#BB2427', fontWeight: '400', fontSize: 12}}>
              Add Photo
            </Text>
          </View>

          <FlatList
            data={options}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => (
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <TouchableOpacity
                  style={{
                    marginTop: 10,
                    borderColor: 'black',
                    borderWidth: 1,
                    height: 24,
                    width: 24,
                  }}
                  onPress={() => handleCheckbox(item)}>
                  {item.isChecked ? (
                    <Icon name="check" size={25} color="#000000" />
                  ) : null}
                </TouchableOpacity>
                <Text
                  style={{
                    color: '#201F26',
                    fontSize: 14,
                    fontWeight: '400',
                    marginTop: 10,
                    marginLeft: 15,
                  }}>
                  {item.name}
                </Text>
              </View>
            )}
          />

          <Text
            style={{
              color: '#BB2427',
              fontWeight: '600',
              fontSize: 12,
              marginTop: 20,
            }}>
            Catatan
          </Text>
          <View
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 10,
              marginVertical: 10,
              paddingHorizontal: 5,
            }}>
            <TextInput
              value={catatan}
              onChangeText={text => setCatatan(text)}
              style={{textAlignVertical: 'top', height: 93}}
              placeholder="Cth : ingin ganti sol baru"
              multiline={true}
            />
          </View>
        </View>
        <TouchableOpacity
          onPress={storeData}
          style={{
            backgroundColor: '#BB2427',
            alignSelf: 'center',
            width: '90%',
            padding: 15,
            alignItems: 'center',
            marginHorizontal: 10,
            borderRadius: 10,
            marginBottom: 30,
          }}>
          <Text style={{fontSize: 16, fontWeight: '700', color: '#FFFFFF'}}>
            Masukkan Keranjang
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Form;
