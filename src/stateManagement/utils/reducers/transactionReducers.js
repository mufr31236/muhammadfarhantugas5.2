const initialState = {
  reservations: [],
};

const transactionReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_RESERVATION':
      return {
        ...state,
        reservations: action.data,
      };

    case 'DELETE_RESERVATION':
      var newData = [...state.reservations];
      var findIndex = state.reservations.findIndex(it => {
        return it.id == action.data.id;
      });
      newData.splice(findIndex, 1);
      return {
        ...state,
        reservations: newData,
      };
    default:
      return state;
  }
};

export default transactionReducer;
