const initialState = {
  products: [],
};

const productsReducer = (state = initialState, action) => {
  let newProducts = [...state.products];
  switch (action.type) {
    case 'ADD_PRODUCT':
      return {
        ...state,
        products: action.data,
      };

    case 'DELETE_PRODUCT':
      var newData = [...state.products];
      var findIndex = state.products.findIndex(it => {
        return it.id == action.data.id;
      });
      newData.splice(findIndex, 1);
      return {
        ...state,
        products: newData,
      };
    default:
      return state;
  }
};

export default productsReducer;
