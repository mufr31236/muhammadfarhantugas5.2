const initialState = {
  store: [],
};

const storeReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_STORE':
      return {
        ...state,
        store: action.data,
      };

    case 'DELETE_STORE':
      var newData = [...state];
      return {
        ...state,
      };

    default:
      return state;
  }
};

export default storeReducers;
