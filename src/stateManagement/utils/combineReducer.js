import {combineReducers} from 'redux';
import productsReducer from './reducers/productReducers';
import transactionReducer from './reducers/transactionReducers';
import storeReducers from './reducers/storeReducers';

const rootReducer = combineReducers({
  products: productsReducer,
  reservation: transactionReducer,
  store: storeReducers,
});

export default rootReducer;
