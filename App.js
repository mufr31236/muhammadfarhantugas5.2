import React from 'react';
import { SafeAreaView, StatusBar } from "react-native";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import Routing from './src/navigation/Routing';
import {persistor, store} from './src/stateManagement/utils/store';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <SafeAreaView style={{flex: 1}}>
          <StatusBar barStyle={'dark-content'} />
          <Routing/>
        </SafeAreaView>
      </PersistGate>
    </Provider>
  );
};

export default App;